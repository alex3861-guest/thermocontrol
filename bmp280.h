/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BMP280_H
#define BMP280_H

#include <QWidget>
#include "commod_def.h"
#include <QJsonObject>
#include <QJsonArray>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
namespace Ui {
class BMP280;
}

class BMP280 : public QWidget
{
    Q_OBJECT

public:
    explicit BMP280(QWidget *parent = nullptr);
    ~BMP280();
    ErrorCode   getPV(uint8_t channel,double* pv);
    void        getConfig(QJsonObject* jObject);
    ErrorCode   setConfig(QJsonObject* jObject);
    void        setTag(QString tag);

private slots:
    void on_pbQuit_clicked();

    void on_pbSetDeviceConfig_clicked();

    void on_pbGetDeviceConfig_clicked();

    void on_sbAddress_valueChanged(int arg1);

private:
    enum BMP280_Regisers
    {
        Register_CompensationParams = 0x88,
        Register_ID = 0xD0,
        Register_Reset = 0xE0,
        Register_Status = 0xF3,
        Register_Ctrl_Meas = 0xF4,
        Register_Config = 0xF5,
        Register_Press_MSB = 0xF7,
        Register_Press_LSB = 0xF8,
        Register_Press_xLSB = 0xF9,
        Register_Temp_MSB = 0xFA,
        Register_Temp_LSB = 0xFB,
        Register_Temp_xLSB = 0xFC
    };
    Ui::BMP280 *ui;
    ErrorCode       DeviceInit();
    ErrorCode       RequestConfiguration();
//    ErrorCode       ReadConversionRegister(int16_t *data);
//    ErrorCode       SetRegister(uint8_t devRegister);
    ErrorCode       SetDeviceConfig();
//    ErrorCode       StartSingleConversion();
//    ErrorCode       ReadRegister(uint8_t devRegister, void *data);
//    ErrorCode       WriteRegister(uint8_t devRegister, void *data);
//    void            DeviceConfig2Gui(uint16_t *config);
    void            Config2json(QJsonObject* jObject);
    ErrorCode       json2Config(QJsonObject* jObject);
//    uint16_t        ConfigAssembly();
//    double          Linearize(double pv);
    ErrorCode   getCompensationParams();
    uint16_t    getUint16(uint8_t lsb,uint8_t msb);
    int16_t     getInt16(uint8_t lsb,uint8_t msb);
    double      getPV_Temp(int32_t raw_data);
    double      getPV_Press(int32_t raw_data);

//    uint16_t    vDeviceConfig;
    int         vIOfileDescriptor;
    uint8_t     vAddress;
    uint8_t     vRegister;
    uint16_t    dig_T1,dig_P1;
    int16_t     dig_T2,dig_T3,dig_P2,dig_P3,dig_P4,dig_P5,dig_P6,dig_P7,dig_P8,dig_P9;
    int32_t     T_fine;
};

#endif // BMP280_H
