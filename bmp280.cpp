/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bmp280.h"
#include "ui_bmp280.h"

BMP280::BMP280(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BMP280)
{
    ui->setupUi(this);
}

BMP280::~BMP280()
{
    delete ui;
}

ErrorCode BMP280::getPV(uint8_t channel, double *pv)
{
    if(channel>1)
        return Error_ChannelOutOfRange;
    uint8_t data[3];
    uint8_t reg;
    switch (channel)
    {
    case 0:
        reg = Register_Press_MSB;
        break;
    case 1:
        reg = Register_Temp_MSB;
    }
    write(vIOfileDescriptor,&reg,1);
    if(read(vIOfileDescriptor,data,3)<3)
        return Error_Read;
    int32_t XDvalue = (data[0]<<12)+(data[1]<<4)+(data[2]>>4);
    switch(channel)
    {
    case 0:
        ui->lb_ch0_XD->setText(QString().setNum(XDvalue));
        *pv = getPV_Press(XDvalue);
        ui->lb_ch0_PV->setText(QString().setNum(*pv)+" "+ui->le_ch0_PV_unit->text());
        break;
    case 1:
        ui->lb_ch1_XD->setText(QString().setNum(XDvalue));
        *pv = getPV_Temp(XDvalue);
        ui->lb_ch1_PV->setText(QString().setNum(*pv)+" "+ui->le_ch1_PV_unit->text());
    }
    return No_Error;
}

void BMP280::getConfig(QJsonObject *jObject)
{
    Config2json(jObject);
}

ErrorCode BMP280::setConfig(QJsonObject *jObject)
{
    ErrorCode err;
    err = json2Config(jObject);
    if(err!=No_Error)
        return err;
//    vDeviceConfig = ConfigAssembly();
    return DeviceInit();
}

void BMP280::setTag(QString tag)
{
    ui->leTag->setText(tag);
}

void BMP280::on_pbQuit_clicked()
{
    this->hide();
}

ErrorCode BMP280::DeviceInit()
{
    this->setWindowTitle(ui->leTag->text()+": BMP280 Configuration Module");
    if(vIOfileDescriptor > 0)
        ::close(vIOfileDescriptor);
    vIOfileDescriptor = open(ui->leIOfile->text().toStdString().data(), O_RDWR);

    if(vIOfileDescriptor<0)
        return Error_FileOpen;

    if ((vAddress > 127) |
        (vAddress < 8))
        return Error_AddresOutOfRange;

    ioctl(vIOfileDescriptor,I2C_SLAVE,vAddress);
    getCompensationParams();
    return  SetDeviceConfig();
}

ErrorCode BMP280::RequestConfiguration()
{
    uint8_t deviceConfig[2];
    uint8_t reg = Register_Ctrl_Meas;
    write(vIOfileDescriptor,&reg,1);
    if(read(vIOfileDescriptor,deviceConfig,2)<2)
        return Error_Read;
    ui->cbPressureOversampling->setCurrentIndex((deviceConfig[0]>>2)&7);
    ui->cbTemperatureOversampling->setCurrentIndex((deviceConfig[0]>>5)&7);
    ui->cbFilterTime->setCurrentIndex((deviceConfig[1]>>2)&7);
    ui->cbInactiveDureation->setCurrentIndex((deviceConfig[1]>>5)&7);
    return  No_Error;
}

ErrorCode BMP280::SetDeviceConfig()
{
    uint8_t *data;
    data = new uint8_t[2];
    data[0] = Register_Ctrl_Meas;
    data[1] = static_cast<uint8_t>((ui->cbTemperatureOversampling->currentIndex() << 5) |
                                (ui->cbPressureOversampling->currentIndex() << 2) | 3);
    if(write(vIOfileDescriptor,data,2)<2)
        return Error_Write;

    data[0] = Register_Config;
    data[1] = static_cast<uint8_t>((ui->cbInactiveDureation->currentIndex() << 5) |
                                             (ui->cbFilterTime->currentIndex() << 2) );
    if(write(vIOfileDescriptor,data,2)<2)
        return Error_Write;
    return No_Error;
}

//ErrorCode BMP280::ReadRegister(uint8_t devRegister, void *data)
//{

//}

//ErrorCode BMP280::WriteRegister(uint8_t devRegister, void *data)
//{

//}

//void BMP280::DeviceConfig2Gui(uint16_t *config)
//{

//}

void BMP280::Config2json(QJsonObject *jObject)
{
    QJsonObject newJObject;
    newJObject.insert("device",QJsonValue("BMP280"));
    newJObject.insert("I/O file",QJsonValue(ui->leIOfile->text()));
    newJObject.insert("address",QJsonValue(ui->sbAddress->value()));
    newJObject.insert("Temperature oversampling",QJsonValue(ui->cbTemperatureOversampling->currentText()));
    newJObject.insert("Pressure oversapmpling",QJsonValue(ui->cbPressureOversampling->currentText()));
    newJObject.insert("Inactive duration",QJsonValue(ui->cbInactiveDureation->currentText()));
    newJObject.insert("Filter time",QJsonValue(ui->cbFilterTime->currentText()));
//    newJObject.insert("channel 0 - XD scale 0%",QJsonValue(ui->sb_ch0_XD_0->value()));
//    newJObject.insert("channel 0 - XD scale 100%",QJsonValue(ui->sb_ch0_XD_100->value()));
//    newJObject.insert("channel 0 - PV scale 0%",QJsonValue(ui->ds_ch0_PV_0->value()));
//    newJObject.insert("channel 0 - PV scale 100%",QJsonValue(ui->ds_ch0_PV_100->value()));
    newJObject.insert("channel 0 - PV unit",QJsonValue(ui->le_ch0_PV_unit->text()));
//    newJObject.insert("channel 1 - XD scale 0%",QJsonValue(ui->sb_ch1_XD_0->value()));
//    newJObject.insert("channel 1 - XD scale 100%",QJsonValue(ui->sb_ch1_XD_100->value()));
//    newJObject.insert("channel 1 - PV scale 0%",QJsonValue(ui->ds_ch1_PV_0->value()));
//    newJObject.insert("channel 1 - PV scale 100%",QJsonValue(ui->ds_ch1_PV_100->value()));
    newJObject.insert("channel 1 - PV unit",QJsonValue(ui->le_ch1_PV_unit->text()));
    jObject->insert(ui->leTag->text(),QJsonValue(newJObject));

}

ErrorCode BMP280::json2Config(QJsonObject *jObject)
{
    QJsonObject *jObjectTmp = new QJsonObject(jObject->value(ui->leTag->text()).toObject());
    if(jObjectTmp->value("device").toString()!="BMP280")
        return Error_DeviceMismatch;
    ui->leIOfile->setText(jObjectTmp->value("I/O file").toString());
    ui->sbAddress->setValue(jObjectTmp->value("address").toInt());
    ui->cbTemperatureOversampling->setCurrentText(jObjectTmp->value("Temperature oversampling").toString());
    ui->cbPressureOversampling->setCurrentText(jObjectTmp->value("Pressure oversapmpling").toString());
    ui->cbInactiveDureation->setCurrentText(jObjectTmp->value("Inactive duration").toString());
    ui->cbFilterTime->setCurrentText(jObjectTmp->value("Filter time").toString());
//    ui->sb_ch0_XD_0->setValue(jObjectTmp->value("channel 0 - XD scale 0%").toInt());
//    ui->sb_ch0_XD_100->setValue(jObjectTmp->value("channel 0 - XD scale 100%").toInt());
//    ui->ds_ch0_PV_0->setValue(jObjectTmp->value("channel 0 - PV scale 0%").toDouble());
//    ui->ds_ch0_PV_100->setValue(jObjectTmp->value("channel 0 - PV scale 100%").toDouble());
    ui->le_ch0_PV_unit->setText(jObjectTmp->value("channel 0 - PV unit").toString());
//    ui->sb_ch1_XD_0->setValue(jObjectTmp->value("channel 1 - XD scale 0%").toInt());
//    ui->sb_ch1_XD_100->setValue(jObjectTmp->value("channel 1 - XD scale 100%").toInt());
//    ui->ds_ch1_PV_0->setValue(jObjectTmp->value("channel 1 - PV scale 0%").toDouble());
//    ui->ds_ch1_PV_100->setValue(jObjectTmp->value("channel 1 - PV scale 100%").toDouble());
    ui->le_ch1_PV_unit->setText(jObjectTmp->value("channel 1 - PV unit").toString());
    return No_Error;
}

ErrorCode BMP280::getCompensationParams()
{
    uint8_t data[24];
    uint8_t reg = Register_CompensationParams;
    write(vIOfileDescriptor,&reg,1);
    if(read(vIOfileDescriptor,data,24)<24)
        return Error_Read;

    dig_T1 = getUint16(data[0],data[1]);
    dig_T2 = getInt16(data[2],data[3]);
    dig_T3 = getInt16(data[4],data[5]);
    dig_P1 = getUint16(data[6],data[7]);
    dig_P2 = getInt16(data[8],data[9]);
    dig_P3 = getInt16(data[10],data[11]);
    dig_P4 = getInt16(data[12],data[13]);
    dig_P5 = getInt16(data[14],data[15]);
    dig_P6 = getInt16(data[16],data[17]);
    dig_P7 = getInt16(data[18],data[19]);
    dig_P8 = getInt16(data[20],data[21]);
    dig_P9 = getInt16(data[22],data[23]);
    return  No_Error;
}

uint16_t BMP280::getUint16(uint8_t lsb, uint8_t msb)
{
    return static_cast<uint16_t>(lsb + (msb<<8));
}

int16_t BMP280::getInt16(uint8_t lsb, uint8_t msb)
{
    return static_cast<int16_t>(lsb + (msb<<8));
}

double BMP280::getPV_Temp(int32_t raw_data)
{
    int32_t var1,var2,T;
    var1 = static_cast<int32_t>((((raw_data>>3)-(dig_T1<<1))*dig_T2)>>11);
    var2 = static_cast<int32_t>(((((raw_data>>4)-dig_T1)*((raw_data>>4)-dig_T1)>>12)*dig_T3)>>14);
    T_fine = var1 + var2;
    T = (T_fine*5+128)>>8;
    return T/100.0;
}

double BMP280::getPV_Press(int32_t raw_data)
{
    int64_t var1, var2, P;
    var1 = static_cast<int64_t>(T_fine) - 128000;
    var2 = var1*var1*dig_P6;
    var2 += (var1 * dig_P5)<<17;
    var2 += (static_cast<int64_t>(dig_P4) << 35);
    var1 = ((var1 * var1 *dig_P3)>>8) + ((var1 * dig_P2)<<12);
    var1 = ((int64_t(1) << 47) + var1) * dig_P1 >> 33;
    if(var1 == 0)
        return 0;
    P = 1048576 - raw_data;
    P = (((P<<31)-var2)*3125)/var1;
    var1 = (dig_P9 * (P>>13) * (P>>13))>>25;
    var2 = (dig_P8 * P)>>19;
    P = ((P + var1 + var2)>>8) + (dig_P7<<4);
    return P/25600.0;
}

//uint16_t BMP280::ConfigAssembly()
//{
//    return     static_cast<uint16_t>(ui->cbTemperatureOversampling->currentIndex() <<13 |
//                                     (ui->cbPressureOversampling->currentIndex()) << 10 |
//                                     (ui->cbInactiveDureation->currentIndex()) << 5 |
//                                     (ui->cbFilterTime->currentIndex()) << 2 );
//}

void BMP280::on_pbSetDeviceConfig_clicked()
{
    SetDeviceConfig();
}

void BMP280::on_pbGetDeviceConfig_clicked()
{
    RequestConfiguration();
}

void BMP280::on_sbAddress_valueChanged(int arg1)
{
    vAddress = static_cast<uint8_t>(arg1);
}
