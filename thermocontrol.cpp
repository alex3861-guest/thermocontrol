/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "thermocontrol.h"
#include "ui_thermocontrol.h"

ThermoControl::ThermoControl(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ThermoControl)
{
    ui->setupUi(this);
    AI_0 = new ADS111X;
    AO_0 = new MCP4725;
    PID_0 = new PIDcontrol;
    AI_1 = new BMP280;
    AI_0->setWindowFlag(Qt::WindowStaysOnTopHint);
    AO_0->setWindowFlag(Qt::WindowStaysOnTopHint);
    AI_1->setWindowFlag(Qt::WindowStaysOnTopHint);

    AI_0->setTag("AI-01");
    AO_0->setTag("AO-01");
    AI_1->setTag("AI_02");
    vPID_tag = "PID-01";
    ConfigLoad();
    connect(&vScanTimer,SIGNAL(timeout()),this,SLOT(on_ScanTimer_timeout()));
    vScanTimer.setInterval(ui->sbIOScanTime->value());
    vScanTimer.setSingleShot(false);
    vScanTimer.start();
}

ThermoControl::~ThermoControl()
{
    delete AI_0;
    delete AO_0;
    delete PID_0;
    delete AI_1;
    delete ui;
}

void ThermoControl::on_pbAnalogInputSetup_clicked()
{
    AI_0->show();
}

void ThermoControl::on_pbAnalogOutputSetup_clicked()
{
    AO_0->show();
}

void ThermoControl::on_pbSaveConfig_clicked()
{
    GetConfigs();
    ConfigSave();
}

void ThermoControl::ConfigLoad()
{
    QString s = QDir().home().path() + "/ProcessControl/";
    QDir().mkdir(s);
    vConfigFileName = QFileDialog::getOpenFileName(this,"Open config file",s,"");
    if(vConfigFileName == "")
    {
        GetConfigs();
        return;
    }
    QFile *File = new QFile(vConfigFileName);
    if(!File->open(QFile::ReadOnly))
       return;
    QByteArray arr = File->readAll();
    QJsonDocument jDoc = jDoc.fromJson(arr);
    File->close();
    vConfig = jDoc.object();
    s = vConfig.value("file type").toString();
    if(s != "Process Control config")
    {
        QMessageBox::critical(this,"Critical Error","Configuration File is not\n "
                                                    "a Process Control Config file!!!\n "
                                                    "Deafault configuration will used instead...");
        GetConfigs();
        return;
    }
    SetControlConfig();
    SetIOconfig();
//    ui->
}

void ThermoControl::ConfigSave()
{
    QString s = QDir().home().path() + "/ProcessControl/";
    QString fileName = QFileDialog::getSaveFileName(this,"Save config file",s,"*.cfg");
    if(fileName == "")
        return;
    QFile *File = new QFile(fileName);
    if(!File->open(QFile::WriteOnly))
        return;
    QJsonDocument jDoc;
    jDoc.setObject(vConfig);
    QByteArray arr = jDoc.toJson();
    qint64 byteNum = File->write(arr);
    File->close();
    if(byteNum<0)
        return;
    vConfigFileName = fileName;
}

void ThermoControl::GetConfigs()
{
    QJsonObject jobj;
    jobj.insert("SP",QJsonValue(vSP));
    jobj.insert("Gain",QJsonValue(ui->dsGain->value()));
    jobj.insert("Reset Time",QJsonValue(ui->dsResetTime->value()));
    jobj.insert("Derivative Time",QJsonValue(ui->dsDerivativeTime->value()));
    jobj.insert("Scan Time",QJsonValue(ui->sbIOScanTime->value()));
    jobj.insert("PV scale 0%",QJsonValue(ui->dsPV_0->value()));
    jobj.insert("PV scale 100%",QJsonValue(ui->dsPV_100->value()));
    jobj.insert("OUT scale 0%",QJsonValue(ui->dsOUT_0->value()));
    jobj.insert("OUT scale 100%",QJsonValue(ui->dsOUT_100->value()));
    jobj.insert("DeadBand",QJsonValue(ui->dsDeadBand->value()));
    jobj.insert("OUT inverted",QJsonValue(ui->chbOUTinverted->isChecked()));
    vConfig.insert("file type","Process Control config");
    vConfig.insert(vPID_tag,jobj);
    AI_0->getConfig(&vConfig);
    AO_0->getConfig(&vConfig);
    AI_1->getConfig(&vConfig);
}

void ThermoControl::SetControlConfig()
{
    QJsonObject jObj = vConfig.value(vPID_tag).toObject();
    vSP = jObj.value("SP").toDouble();
    ui->dsGain->setValue(jObj.value("Gain").toDouble());
    ui->dsResetTime->setValue(jObj.value("Reset Time").toDouble());
    ui->dsDerivativeTime->setValue(jObj.value("Derivative Time").toDouble());
    ui->sbIOScanTime->setValue(jObj.value("Scan Time").toInt());
    ui->dsPV_0->setValue(jObj.value("PV scale 0%").toDouble());
    ui->dsPV_100->setValue(jObj.value("PV scale 100%").toDouble());
    ui->dsOUT_0->setValue(jObj.value("OUT scale 0%").toDouble());
    ui->dsOUT_100->setValue(jObj.value("OUT scale 100%").toDouble());
    ui->dsDeadBand->setValue(jObj.value("DeadBand").toDouble());
    ui->chbOUTinverted->setChecked(jObj.value("OUT inverted").toBool());
    SetPIDconfig();
}

void ThermoControl::SetIOconfig()
{
    ErrorCode err = AI_0->setConfig(&vConfig);
    if(err!=No_Error)
        ui->statusBar->showMessage("AI_0: FAILED TO SET CONFIG",5000);
    err = AO_0->setConfig(&vConfig);
    if(err!=No_Error)
        ui->statusBar->showMessage("AO_0: FAILED TO SET CONFIG",5000);
    err = AI_1->setConfig(&vConfig);
    if(err!=No_Error)
        ui->statusBar->showMessage("AI_1: FAILED TO SET CONFIG",5000);
}

void ThermoControl::SetPIDconfig()
{
    PID_0->setGain(ui->dsGain->value());
    PID_0->setSetpoint(vSP);
    PID_0->setResetTime(ui->dsResetTime->value());
    PID_0->setDerivativeTime(ui->dsDerivativeTime->value());
    PID_0->setOutputInversed(ui->chbOUTinverted->isChecked());
    PID_0->setDeadBand(ui->dsDeadBand->value());
    SetPVscale();
    SetOUTscale();
}

void ThermoControl::SetPVscale()
{
    PID_0->setPV_Range(ui->dsPV_0->value(),
                       ui->dsPV_100->value());
    ui->pbPV->setRange(static_cast<int>(ui->dsPV_0->value()),
                       static_cast<int>(ui->dsPV_100->value()));
    ui->slSP->setRange(static_cast<int>(ui->dsPV_0->value()),
                       static_cast<int>(ui->dsPV_100->value()));
}

void ThermoControl::SetOUTscale()
{
    PID_0->setOUT_Range(ui->dsOUT_0->value(),
                       ui->dsOUT_100->value());
    ui->pbOUT->setRange(static_cast<int>(ui->dsOUT_0->value()),
                       static_cast<int>(ui->dsOUT_100->value()));
    ui->slOUT->setRange(static_cast<int>(ui->dsOUT_0->value()),
                       static_cast<int>(ui->dsOUT_100->value()));
}


void ThermoControl::on_pbLoadConfig_clicked()
{
    ConfigLoad();
}

void ThermoControl::on_ScanTimer_timeout()
{
    double pv;
    ErrorCode err = AI_0->getPV(&vPV);

    if(err != No_Error)
        ui->statusBar->showMessage("PV read error!!!",5000);
    err = AI_1->getPV(1,&pv);
    if(err != No_Error)
        ui->statusBar->showMessage("AI-1 read error!!!",5000);
    else
        vPV += pv;
    if(ui->rbOutOfService->isChecked())
        return;
    ui->lbPV->setText(QString().setNum(vPV,'f',2));
    ui->pbPV->setValue(static_cast<int>(vPV));
    ui->slSP->setValue(static_cast<int>(vSP));
    ui->lbSP->setNum(vSP);
    if(ui->rbPIDmodeAuto->isChecked())
    {
        PID_0->setSetpoint(vSP);
        vOUT = PID_0->calculate(vPV);
    }
    err = AO_0->setPV(vOUT);
    ui->lbOUT->setText(QString().setNum(vOUT,'f',2));
    ui->pbOUT->setValue(static_cast<int>(vOUT));
    ui->slOUT->setValue(static_cast<int>(vOUT));

    if(err != No_Error)
        ui->statusBar->showMessage("Failed to set Output!!!",5000);
    AI_1->getPV(0,&pv);
}

void ThermoControl::on_slOUT_valueChanged(int value)
{
    if(ui->rbPIDmodeAuto->isChecked())
        return;
    vOUT = static_cast<double>(value);
}

void ThermoControl::on_rbPIDmodeAuto_toggled(bool checked)
{
    if(checked)
    {
        ui->slOUT->setEnabled(false);
        PID_0->setOUT_Val(vOUT);
    }
    else
        ui->slOUT->setEnabled(true);
}

void ThermoControl::on_dsGain_editingFinished()
{
    PID_0->setGain(ui->dsGain->value());
}

void ThermoControl::on_dsResetTime_editingFinished()
{
    PID_0->setResetTime(ui->dsResetTime->value());
}

void ThermoControl::on_dsDerivativeTime_editingFinished()
{
    PID_0->setDerivativeTime(ui->dsDerivativeTime->value());
}

void ThermoControl::on_dsDeadBand_editingFinished()
{
    PID_0->setDeadBand(ui->dsDeadBand->value());
}

void ThermoControl::on_dsPV_0_editingFinished()
{
    SetPVscale();
}

void ThermoControl::on_dsPV_100_editingFinished()
{
    SetPVscale();
}

void ThermoControl::on_dsOUT_0_editingFinished()
{
    SetOUTscale();
}

void ThermoControl::on_dsOUT_100_editingFinished()
{
    SetOUTscale();
}

void ThermoControl::on_chbOUTinverted_toggled(bool checked)
{
    PID_0->setOutputInversed(checked);
}

void ThermoControl::on_slSP_valueChanged(int value)
{
    vSP = static_cast<double>(value);
}

void ThermoControl::on_sbIOScanTime_valueChanged(int arg1)
{
    vScanTimer.setInterval(arg1);
}

void ThermoControl::on_pbEditSP_clicked()
{
    vSP = QInputDialog::getDouble(this,"Enter SP value","lable",vSP,ui->dsPV_0->value(),ui->dsPV_100->value());
}

void ThermoControl::on_pbAmbientTemperatureSetup_clicked()
{
    AI_1->show();
}
