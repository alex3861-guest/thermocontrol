#-------------------------------------------------
#
# Project created by QtCreator 2020-10-04T10:23:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = thermocontrol
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

icon.path = /opt/$${TARGET}
icon.files = thermocontrol_64.png

INSTALLS += icon

launcher.path = /usr/share/applications
launcher.files = thermocontrol.desktop

INSTALLS += launcher

docs.path = /opt/$${TARGET}/share/doc
docs.files = docs/*
docs.files += debian/changelog
docs.files += debian/copyright

INSTALLS += docs


CONFIG += c++11

SOURCES += \
        main.cpp \
        thermocontrol.cpp \
    mcp4725.cpp \
    ads111x.cpp \
    pidcontrol.cpp \
    bmp280.cpp

HEADERS += \
        thermocontrol.h \
    mcp4725.h \
    ads111x.h \
    commod_def.h \
    pidcontrol.h \
    bmp280.h

FORMS += \
        thermocontrol.ui \
    mcp4725.ui \
    ads111x.ui \
    bmp280.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#unix|win32: LIBS += -lnfft3

DISTFILES += \
    debian/changelog \
    debian/control \
    debian/patches/series \
    debian/rules \
    debian/source/control \
    debian/source/format \
    debian/source/local-options \
    debian/source/options \
    debian/source/patch-header \
    debian/tests/control \
    debian/upstream/metadata \
    debian/watch \
    docs/LICENSE \
    docs/README \
    thermocontrol.desktop \
    thermocontrol_128.png \
    thermocontrol_64.png
