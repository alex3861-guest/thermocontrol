/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MCP4725_H
#define MCP4725_H

#include <QWidget>
#include <QJsonObject>
#include <QMessageBox>
#include "commod_def.h"

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
namespace Ui {
class MCP4725;
}

class MCP4725 : public QWidget
{
    Q_OBJECT

public:
    explicit MCP4725(QWidget *parent = nullptr);
    ~MCP4725();
    ErrorCode   setPV(double pv);
    ErrorCode   writeToNVM(double pv);
    void        getConfig(QJsonObject* jObject);
    ErrorCode   setConfig(QJsonObject* jObject);
    void        setTag(QString tag);


signals:
    void ConfigurationChanged();

private slots:
    void on_btApply_clicked();

    void on_btSave_clicked();

    void on_btQuit_clicked();

    void on_pbWriteToNVM_clicked();

    void on_pbReadFromNVM_clicked();

    void on_sbAddress_valueChanged(int arg1);

    void on_pbSetManualInputValue_clicked();

private:
    enum MCP4725_WriteCommand
    {
        Fast_Mode = 0,
        Write_Register = 0x40,
        Write_NVM_AND_Reg = 0x60
    };

    enum MCP4725_PowerDownFlags
    {
        NormalMode = 0,
        PowerDown_1K_Fast = 0x10,
        PowerDown_100K_Fast = 0x20,
        PowerDown_500K_Fast = 0x30,
        PowerDown_1K = 0x02,
        PowerDown_100K = 0x04,
        PowerDown_500K = 0x06
    };

    Ui::MCP4725 *ui;

    ErrorCode       DeviceInit();
    ErrorCode       ReadRegister(uint16_t *data, uint16_t *NVM_data);
    ErrorCode       WriteRegister(uint8_t command, uint16_t *data);
    void            Config2json(QJsonObject* jObject);
    ErrorCode       json2Config(QJsonObject* jObject);
    uint16_t*       PV2XD(double *data);
//    ErrorCode

//    uint16_t    vDeviceConfig;
    int         vIOfileDescriptor;
    uint8_t     vAddress;
    uint16_t    *vXDvalue;
//    uint8_t     vRegister;

};

#endif // MCP4725_H
