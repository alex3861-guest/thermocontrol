/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef THERMOCONTROL_H
#define THERMOCONTROL_H

#include <Qt>
#include <QMainWindow>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QInputDialog>
#include <QTimer>
#include "ads111x.h"
#include "mcp4725.h"
#include "bmp280.h"
#include "pidcontrol.h"
#include "commod_def.h"

namespace Ui {
class ThermoControl;
}

class ThermoControl : public QMainWindow
{
    Q_OBJECT

public:
    explicit ThermoControl(QWidget *parent = nullptr);
    ~ThermoControl();

private slots:
    void on_pbAnalogInputSetup_clicked();

    void on_pbAnalogOutputSetup_clicked();

    void on_pbSaveConfig_clicked();

    void on_pbLoadConfig_clicked();
    void on_ScanTimer_timeout();        //scan timer

    void on_slOUT_valueChanged(int value);

    void on_rbPIDmodeAuto_toggled(bool checked);

    void on_dsGain_editingFinished();

    void on_dsResetTime_editingFinished();

    void on_dsDerivativeTime_editingFinished();

    void on_dsDeadBand_editingFinished();

    void on_dsPV_0_editingFinished();

    void on_dsPV_100_editingFinished();

    void on_dsOUT_0_editingFinished();

    void on_dsOUT_100_editingFinished();

    void on_chbOUTinverted_toggled(bool checked);

    void on_slSP_valueChanged(int value);

    void on_sbIOScanTime_valueChanged(int arg1);

    void on_pbEditSP_clicked();

    void on_pbAmbientTemperatureSetup_clicked();

private:
    void    ConfigLoad();
    void    ConfigSave();
    void    GetConfigs();
    void    SetControlConfig();
    void    SetIOconfig();
    void    SetPIDconfig();
    void    SetPVscale();
    void    SetOUTscale();

    Ui::ThermoControl *ui;
    ADS111X *AI_0;
    MCP4725 *AO_0;
    BMP280  *AI_1;

    PIDcontrol  *PID_0;
    QJsonObject   vConfig;
    QString vConfigFileName;
    QString vPID_tag;
    QTimer  vScanTimer;
    double  vOUT;
    double  vPV;
    double  vSP;
};

#endif // THERMOCONTROL_H
