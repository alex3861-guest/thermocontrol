/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "pidcontrol.h"

PIDcontrol::PIDcontrol()
{
    vIntegrative = 0;
    vLastOutputVal = 0;
    vLastError = 0;
    vLastInputVal = 0;
    vSetpoint = 0;
    vInputRange = 100;
    vOutputRange = 100;
    vOUT_0 = 0;
    vOUT_100 = 100;
    setOutputInversed(false);
}

void PIDcontrol::setGain(double gain)
{
    this->vGain = gain;
}

void PIDcontrol::setResetTime(double time)
{
    vResetTime = time;
}

void PIDcontrol::setDerivativeTime(double time)
{
    vDerivativeTime = time;
}

void PIDcontrol::setSetpoint(double Setpoint)
{
    vSetpoint = Setpoint;
}

void PIDcontrol::setPV_Range(double PV_0, double PV_100)
{
    vPV_0 = PV_0;
    vPV_100 = PV_100;
    vInputRange = PV_100 - PV_0;
}

void PIDcontrol::setOUT_Range(double OUT_0, double OUT_100)
{
    vOUT_0 = OUT_0;
    vOUT_100 = OUT_100;
    vOutputRange = OUT_100 - OUT_0;
}

void PIDcontrol::setOUT_Val(double outputVal)
{
    vLastOutputVal = outputVal;
}

void PIDcontrol::setOutputInversed(bool Inversed)
{
    vOutputInversed = Inversed;
    if(Inversed)
        vOutputInversionK = -1.0;
    else
        vOutputInversionK = 1.0;
}

void PIDcontrol::setDeadBand(double value)
{
    vDeadBand = value;
}

double PIDcontrol::calculate(double inputVal)
{
    if(isnan(inputVal))
        return vLastOutputVal;
    QDateTime currentDateTime = QDateTime().currentDateTime();
    double error,RelativeError,Der,dOutput,Output;
    qint64 dTime = vLastDateTime.msecsTo(currentDateTime);
    double dT = dTime/1000.0;
//    error = (vSetpoint - inputVal);
    error = vOutputInversionK*(vSetpoint - inputVal);
    RelativeError = error/vInputRange;
    if(isnan(vIntegrative))
        vIntegrative = 0;
    if(fabs(error) > (vDeadBand/2.0))
    {
        if(dT < vResetTime)
            vIntegrative += RelativeError*dT/vResetTime*vGain;
    }
    Der = ((inputVal - vLastInputVal)/vInputRange) * vDerivativeTime / dT;
    if(isnan(Der))
        Der = 0.0;
//    dOutput = vOutputInversionK * (vGain * (RelativeError + Der)+vIntegrative);
    dOutput = vOutputInversionK * (vGain * (RelativeError + Der)+vIntegrative);
    Output = dOutput * vOutputRange;
    if(Output > vOUT_100)
    {
        vLastOutputVal = vOUT_100;
        vIntegrative = (Output - vLastOutputVal)/vOutputRange*vOutputInversionK/vGain - RelativeError - Der;
    }
    else
    {
        if(Output < vOUT_0)
        {
            vLastOutputVal = vOUT_0;
            vIntegrative = (Output - vLastOutputVal)/vOutputRange*vOutputInversionK/vGain - RelativeError - Der;
        }
        else
            vLastOutputVal = Output;
    }
    vLastInputVal = inputVal;
    vLastDateTime = currentDateTime;
    return vLastOutputVal;
}
