/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMOD_DEF_H
#define COMMOD_DEF_H

enum ErrorCode
{
    No_Error,
    Error_Read,
    Error_Write,
    Error_FileOpen,
    Error_AddresOutOfRange,
    Error_DeviceMismatch,
    Error_DevisionByZero,
    Error_ChannelOutOfRange
};

#endif // COMMOD_DEF_H

