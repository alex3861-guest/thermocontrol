/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ADS111X_H
#define ADS111X_H

#include <QWidget>
#include "commod_def.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QTableWidgetItem>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <math.h>

//#include <nfft3.h>
//#include <nfft3mp.h>

namespace Ui {
class ADS111X;
}

class ADS111X : public QWidget
{
    Q_OBJECT

public:
    explicit ADS111X(QWidget *parent = nullptr);
    ~ADS111X();
    ErrorCode   getPV(double* pv);
    void        getConfig(QJsonObject* jObject);
    ErrorCode   setConfig(QJsonObject* jObject);
    void        setTag(QString tag);
    QString     getUnit();

    ErrorCode setLowThreshold(uint16_t *threshold);
    ErrorCode setHighThreshold(uint16_t *threshold);
signals:
    void ConfigurationChanged();

private slots:
    void on_pbApply_clicked();

    void on_pbRefresh_clicked();

    void on_pbQuit_clicked();

    void on_sbAddress_valueChanged(int arg1);
    
    void on_chbEditCalibrationData_toggled(bool checked);

    void on_cbDataProcessing_currentIndexChanged(const QString &arg1);

    void on_cbDataProcessing_currentIndexChanged(int index);

private:
    enum ADS111X_ConfigFlags
    {
        ConfigFlag_StartSingleConv =    0x8000,
        ConfigFlag_Mux_AIN0_AIN1 =      0x0000,
        ConfigFlag_Mux_AIN0_AIN3 =      0x1000,
        ConfigFlag_Mux_AIN1_AIN3 =      0x2000,
        ConfigFlag_Mux_AIN2_AIN3 =      0x3000,
        ConfigFlag_Mux_AIN0_GNS =       0x4000,
        ConfigFlag_Mux_AIN1_GNS =       0x5000,
        ConfigFlag_Mux_AIN2_GNS =       0x6000,
        ConfigFlag_Mux_AIN3_GNS =       0x7000,
        ConfigFlag_PGA_6144mV =         0x0000,
        ConfigFlag_PGA_4096mV =         0x0200,
        ConfigFlag_PGA_2048mV =         0x0400,
        ConfigFlag_PGA_1024mV =         0x0600,
        ConfigFlag_PGA_512mV =          0x0800,
        ConfigFlag_PGA_256mV =          0x0a00,
        ConfigFlag_ContinuousConvMode = 0x0000,
        ConfigFlag_SingleShotMode =     0x0100,
        ConfigFlag_DataRate_8 =         0x0000,
        ConfigFlag_DataRate_16 =        0x0020,
        ConfigFlag_DataRate_32 =        0x0040,
        ConfigFlag_DataRate_64 =        0x0060,
        ConfigFlag_DataRate_128 =       0x0080,
        ConfigFlag_DataRate_250 =       0x00a0,
        ConfigFlag_DataRate_475 =       0x00c0,
        ConfigFlag_DataRate_860 =       0x00e0,
        ConfigFlag_CompModeTraditional= 0x0000,
        ConfigFlag_CompModeWindow =     0x0010,
        ConfigFlag_CompPolLow =         0x0000,
        ConfigFlag_CompPolHigh =        0x0008,
        ConfigFlag_CompLat_NoLatch =    0x0000,
        ConfigFlag_CompLat_Latch =      0x0004,
        ConfigFlag_CompAssert_1 =       0x0000,
        ConfigFlag_CompAssert_2 =       0x0001,
        ConfigFlag_CompAssert_4 =       0x0002,
        ConfigFlag_Comp_Disabled =      0x0003
    };
    enum ADS111X_Register
    {
        Conversion_Register =           0x00,
        Config_Register =               0x01,
        Lo_Threshold_Register =         0x02,
        Hi_Threshold_Register =         0x03
    };
    enum ADS111X_ConfigMasks
    {
        ConfigMask_OperationalStatus =  0x8000,
        ConfigMask_Multiplexer =        0x7000,
        ConfigMask_PGA =                0x0e00,
        ConfigMask_DataRate =           0x00e0,
        ConfigMask_CompQue =            0x0003
    };
    enum ADS111X_OperationalStatus
    {
        ConversionInProgress =          0x0000,
        ConversionCompleted =           0x8000
    };
    enum DataProcessing
    {
        DataProcessing_Linear   = 0,
        DataProcessing_SquerRoot,
        DataProcessing_CalibrationChart
    };

    struct ConvertionData
    {
        double  shiftX;
        double  shiftY;
        double  K;
        double  maxX;
        double  maxY;
        double  *Polinomial_Coefficients;
        double  constArg;
    };

    Ui::ADS111X *ui;
    ErrorCode       DeviceInit();
    ErrorCode       RequestConfiguration();
    ErrorCode       ReadConversionRegister(int16_t *data);
    ErrorCode       ReadLowThRegister(uint16_t *data);
    ErrorCode       ReadHighThRegister(uint16_t *data);
    ErrorCode       WriteLowThRegister(uint16_t *data);
    ErrorCode       WriteHighThRegister(uint16_t *data);

    ErrorCode       SetRegister(uint8_t devRegister);
    ErrorCode       SetDeviceConfig(uint16_t config);
    ErrorCode       StartSingleConversion();
    ErrorCode       ReadRegister(uint8_t devRegister, void *data);
    ErrorCode       WriteRegister(uint8_t devRegister, void *data);
    void            DeviceConfig2Gui(uint16_t *config);
    void            Config2json(QJsonObject* jObject);
    ErrorCode       json2Config(QJsonObject* jObject);
    uint16_t        ConfigAssembly();
//    double          Linearize(double pv);
    double          DataProcessing(int16_t *data);
    double          Convertion_CalibrationChart(int16_t *rawData);
    void            FTCoeffCalc();

    uint16_t    vDeviceConfig;
    int         vIOfileDescriptor;
    uint8_t     vAddress;
    uint8_t     vRegister;
    QList<QList<QTableWidgetItem>>  vItemArray;
//    double          *vPolinomial_Coefficients;
    ConvertionData  vConvertionData;
//    double          shiftX, shiftY;
};

#endif // ADS111X_H
