/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mcp4725.h"
#include "ui_mcp4725.h"

MCP4725::MCP4725(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MCP4725)
{
    ui->setupUi(this);
    vXDvalue = new uint16_t;
}

MCP4725::~MCP4725()
{
    delete ui;
}

ErrorCode MCP4725::setPV(double pv)
{
    if(!ui->rbModeAuto->isChecked())
        return No_Error;
    uint8_t command;
    if(ui->chbFastMode->isChecked())
        command = Fast_Mode;
    else
        command = Write_Register;
    return WriteRegister(command,PV2XD(&pv));
}

ErrorCode MCP4725::writeToNVM(double pv)
{
    return WriteRegister(uint8_t(Write_NVM_AND_Reg),PV2XD(&pv));
}

void MCP4725::getConfig(QJsonObject *jObject)
{
    Config2json(jObject);
}

ErrorCode MCP4725::setConfig(QJsonObject *jObject)
{
    ErrorCode err;
    err = json2Config(jObject);
    if(err!=No_Error)
        return err;
    return DeviceInit();
}

void MCP4725::setTag(QString tag)
{
    ui->leTag->setText(tag);
}

void MCP4725::on_btApply_clicked()
{

}

void MCP4725::on_btSave_clicked()
{

}

void MCP4725::on_btQuit_clicked()
{
    this->hide();
}

ErrorCode MCP4725::DeviceInit()
{
    this->setWindowTitle(ui->leTag->text()+": MPC4725 Configuration Module");
    if(vIOfileDescriptor > 0)
        ::close(vIOfileDescriptor);
    vIOfileDescriptor = open(ui->leIOfile->text().toStdString().data(), O_RDWR);

    if(vIOfileDescriptor<0)
        return Error_FileOpen;
    
    if ((vAddress > 127) |
        (vAddress < 8))
        return Error_AddresOutOfRange;

    ioctl(vIOfileDescriptor,I2C_SLAVE,vAddress);
    return  No_Error;
}

ErrorCode MCP4725::ReadRegister(uint16_t *data, uint16_t *NVM_data)
{
    uint8_t tmp[5];
    if(read(vIOfileDescriptor,tmp,5)<5)
        return Error_Read;
    *NVM_data = static_cast<uint16_t>(((tmp[3]&0x0F)<<8)|tmp[4]);
    *data = static_cast<uint16_t>((tmp[1]<<4)|(tmp[2]>>4));
    return No_Error;
}

ErrorCode MCP4725::WriteRegister(uint8_t command, uint16_t *data)
{
    if(command == Fast_Mode)
    {
        uint8_t tmp[4];
        tmp[0] = static_cast<uint8_t>(*data >> 8);
        tmp[1] = static_cast<uint8_t>(*data & 0xFF);
        tmp[2] = tmp[0];
        tmp[3] = tmp[1];
        if(write(vIOfileDescriptor,tmp,4)<4)
            return Error_Write;
        else
            return No_Error;
    }
    else
    {
        uint8_t tmp[6];

        if(command == Write_NVM_AND_Reg)
            tmp[0] = Write_NVM_AND_Reg;
        else
            tmp[0] = Write_Register;

        tmp[1] = static_cast<uint8_t>(*data >> 4);
        tmp[2] = static_cast<uint8_t>((*data << 4)&0xFF);
        tmp[3] = tmp[0];
        tmp[4] = tmp[1];
        tmp[5] = tmp[2];
        if(write(vIOfileDescriptor,tmp,6)<6)
            return Error_Write;
        else
            return No_Error;
    }
}

void MCP4725::Config2json(QJsonObject *jObject)
{
    QJsonObject newJObject;
    newJObject.insert("device",QJsonValue("MCP4725"));
    newJObject.insert("I/O file",QJsonValue(ui->leIOfile->text()));
    newJObject.insert("address",QJsonValue(ui->sbAddress->value()));
    newJObject.insert("XD scale 0%",QJsonValue(ui->dsXDscale0->value()));
    newJObject.insert("XD scale 100%",QJsonValue(ui->dsXDscale100->value()));
    newJObject.insert("PV scale 0%",QJsonValue(ui->dsPVscale_0->value()));
    newJObject.insert("PV scale 100%",QJsonValue(ui->dsPVscale100->value()));
    newJObject.insert("Fast mode",QJsonValue(ui->chbFastMode->isChecked()));
    newJObject.insert("NVM Data",QJsonValue(ui->sbDatoToNVM->value()));
    jObject->insert(ui->leTag->text(),QJsonValue(newJObject));
}

ErrorCode MCP4725::json2Config(QJsonObject *jObject)
{
    QJsonObject *jObjectTmp = new QJsonObject(jObject->value(ui->leTag->text()).toObject());
    if(jObjectTmp->value("device").toString()!="MCP4725")
        return Error_DeviceMismatch;
    ui->leIOfile->setText(jObjectTmp->value("I/O file").toString());
    ui->sbAddress->setValue(jObjectTmp->value("address").toInt());
    ui->dsXDscale0->setValue(jObjectTmp->value("XD scale 0%").toDouble());
    ui->dsXDscale100->setValue(jObjectTmp->value("XD scale 100%").toDouble());
    ui->dsPVscale_0->setValue(jObjectTmp->value("PV scale 0%").toDouble());
    ui->dsPVscale100->setValue(jObjectTmp->value("PV scale 100%").toDouble());
    ui->chbFastMode->setChecked(jObjectTmp->value("Fast mode").toBool());
    ui->sbDatoToNVM->setValue(jObjectTmp->value("NVM Data").toInt());
    return No_Error;
}

uint16_t *MCP4725::PV2XD(double *data)
{
    double k = (ui->dsXDscale100->value()-ui->dsXDscale0->value()) /
                (ui->dsPVscale100->value()-ui->dsPVscale_0->value());
    *vXDvalue = static_cast<uint16_t>(*data * k+ui->dsXDscale0->value());
    ui->lbCas_IN->setText(QString().setNum(*data,'f',2));
    ui->lbOUT->setText("dec: " + QString().setNum(*vXDvalue) + "\nhex: "+
                       QString().setNum(*vXDvalue,16));
    return vXDvalue;
}

void MCP4725::on_pbWriteToNVM_clicked()
{
    uint16_t tmp = static_cast<uint16_t>(ui->sbDatoToNVM->value());
    if(WriteRegister(uint8_t(Write_NVM_AND_Reg),&tmp)!=No_Error)
        QMessageBox::critical(this,"Error!!!","Failed to write NVM");
}

void MCP4725::on_pbReadFromNVM_clicked()
{
    uint16_t NVMdata, data;
    ReadRegister(&data,&NVMdata);
    ui->sbDatoToNVM->setValue(NVMdata);
}

void MCP4725::on_sbAddress_valueChanged(int arg1)
{
    vAddress = static_cast<uint8_t>(arg1);
}

void MCP4725::on_pbSetManualInputValue_clicked()
{
    if(!ui->rbModeManual)
        return;
    uint16_t data = static_cast<uint16_t>(ui->sbManualInputVal->value());
    WriteRegister(Fast_Mode,&data);
}
