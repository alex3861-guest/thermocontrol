/*
This module is a part of "thermocontrol" programm.
"thermocontrol" is a small GUI programm implementing PID control
through ordinary PC i2c bus.


Copyright (C) <2018>  <alex fomin, fomin_alex@yahoo.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ads111x.h"
#include "ui_ads111x.h"

ADS111X::ADS111X(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ADS111X)
{
    ui->setupUi(this);
    ui->twCalibrationChart->setRowCount(5);
    ui->twCalibrationChart->setColumnCount(2);
    ui->twCalibrationChart->setHorizontalHeaderLabels(QStringList() << "Raw\ndata" << "Actual\nvalue");
    ui->twCalibrationChart->setVerticalHeaderLabels(QStringList() << "0" << "1" << "2" << "3" << "4");
    ui->twCalibrationChart->setEnabled(false);

    vConvertionData.Polinomial_Coefficients = new double[2];
    int data = 0;
    QList<QTableWidgetItem> tmpList;
    QTableWidgetItem item;
    for(int i=0;i<5;i++)
    {
        tmpList.clear();
        item.setText(QString().setNum(data));
        tmpList.append(item);
        tmpList.append(item);
        vItemArray.append(tmpList);
        ui->twCalibrationChart->setItem(i,0,&vItemArray[i][0]);
        ui->twCalibrationChart->setItem(i,1,&vItemArray[i][1]);
        data += 25;
    }
}

ADS111X::~ADS111X()
{
    delete ui;
    delete [] vConvertionData.Polinomial_Coefficients;
}

ErrorCode ADS111X::getPV(double *pv)
{
    int16_t xdValue;
    if(ReadConversionRegister(&xdValue)!=No_Error)
        return Error_Read;
    ui->lbXD->setText("dec: " + QString().setNum(xdValue)+
                      "\nhex: "+QString().setNum(xdValue,16));
    *pv = DataProcessing(&xdValue);
    ui->lbPV->setText(QString().setNum(*pv,'f',2));
    return No_Error;
}

ErrorCode ADS111X::DeviceInit()
{
    this->setWindowTitle(ui->leTag->text()+": ADS111X Configuration Module");

    if(vIOfileDescriptor > 0)
        ::close(vIOfileDescriptor);
    vIOfileDescriptor = open(ui->leIOfile->text().toStdString().data(), O_RDWR);
    
    if(vIOfileDescriptor<0)
        return Error_FileOpen;

    if ((vAddress > 127) |
        (vAddress < 8))
        return Error_AddresOutOfRange;
    
    ioctl(vIOfileDescriptor,I2C_SLAVE,vAddress);

//    FTCoeffCalc();

    return  SetDeviceConfig(vDeviceConfig);
}

void ADS111X::getConfig(QJsonObject *jObject)
{
    Config2json(jObject);
}

ErrorCode ADS111X::setConfig(QJsonObject *jObject)
{
    ErrorCode err;
    err = json2Config(jObject);
    if(err!=No_Error)
        return err;
    vDeviceConfig = ConfigAssembly();
    return DeviceInit();
}

ErrorCode ADS111X::setLowThreshold(uint16_t *threshold)
{
    return WriteRegister(Lo_Threshold_Register,threshold);
}

ErrorCode ADS111X::setHighThreshold(uint16_t *threshold)
{
    return WriteRegister(Hi_Threshold_Register,threshold);
}

void ADS111X::setTag(QString tag)
{
    ui->leTag->setText(tag);
}

QString ADS111X::getUnit()
{
    return ui->lePV_Unit->text();
}

void ADS111X::on_pbApply_clicked()
{
    vDeviceConfig = ConfigAssembly();
    DeviceInit();
    FTCoeffCalc();
}

void ADS111X::on_pbRefresh_clicked()
{
    RequestConfiguration();
}

void ADS111X::on_pbQuit_clicked()
{
    this->hide();
}

ErrorCode ADS111X::RequestConfiguration()
{
    ErrorCode error = ReadRegister(Config_Register,&vDeviceConfig);
    if(error!=No_Error)
        return error;
    DeviceConfig2Gui(&vDeviceConfig);
    return error;
}

ErrorCode ADS111X::ReadConversionRegister(int16_t *data)
{
    return ReadRegister(Conversion_Register,data);
}

ErrorCode ADS111X::ReadLowThRegister(uint16_t *data)
{
    return ReadRegister(Lo_Threshold_Register,data);
}

ErrorCode ADS111X::ReadHighThRegister(uint16_t *data)
{
    return ReadRegister(Hi_Threshold_Register,data);
}

ErrorCode ADS111X::WriteLowThRegister(uint16_t *data)
{
    return WriteRegister(Lo_Threshold_Register,data);
}

ErrorCode ADS111X::WriteHighThRegister(uint16_t *data)
{
    return WriteRegister(Hi_Threshold_Register,data);
}

ErrorCode ADS111X::SetRegister(uint8_t devRegister)
{
    vRegister = devRegister;
    if(write(vIOfileDescriptor,&vRegister,1)<1)
        return Error_Write;
    return No_Error;
}

ErrorCode ADS111X::SetDeviceConfig(uint16_t config)
{
    return  WriteRegister(Config_Register,&config);
}

ErrorCode ADS111X::StartSingleConversion()
{
    return SetDeviceConfig(vDeviceConfig|ConfigFlag_StartSingleConv);
}

ErrorCode ADS111X::ReadRegister(uint8_t devRegister, void *data)
{
    if(vRegister!=devRegister)
        if(SetRegister(devRegister) != No_Error)
            return Error_Write;
    uint16_t Data;
    if(read(vIOfileDescriptor,&Data,2)<2)
        return Error_Read;
    Data = (htons(Data));
    uint16_t *tmp = static_cast<uint16_t *>(data);
    *tmp = Data;
    return No_Error;
}

ErrorCode ADS111X::WriteRegister(uint8_t devRegister, void *data)
{
    uint8_t buffer[3];
    buffer[0] = devRegister;
    uint8_t* Data = static_cast<uint8_t *>(data);
    buffer[1] = Data[1];
    buffer[2] = Data[0];
    vRegister = devRegister;
    if(write(vIOfileDescriptor,buffer,3)<3)
        return Error_Write;
    else
        return No_Error;
}

void ADS111X::DeviceConfig2Gui(uint16_t *config)
{
    ui->cbMuxConfig->setCurrentIndex((*config & ConfigMask_Multiplexer)>>12);
    ui->cbGainConfig->setCurrentIndex((*config & ConfigMask_PGA)>>9);
    ui->cbOperationMode->setCurrentIndex((*config & ConfigFlag_SingleShotMode)>>8);
    ui->cbDataRate->setCurrentIndex((*config & ConfigMask_DataRate)>>5);
    ui->cbCompMode->setCurrentIndex((*config & ConfigFlag_CompModeWindow)>>4);
    ui->cbCompPolarity->setCurrentIndex((*config & ConfigFlag_CompPolHigh)>>3);
    ui->cbCompLatching->setCurrentIndex((*config & ConfigFlag_CompLat_Latch)>>2);
    ui->cbCompQue->setCurrentIndex(vDeviceConfig & ConfigMask_CompQue);
}

void ADS111X::Config2json(QJsonObject *jObject)
{
    QJsonObject newJObject, jObjectTmp;
    newJObject.insert("device",QJsonValue("ADS111X"));
    newJObject.insert("I/O file",QJsonValue(ui->leIOfile->text()));
    newJObject.insert("address",QJsonValue(ui->sbAddress->value()));
    newJObject.insert("Multiplexer",QJsonValue(ui->cbMuxConfig->currentText()));
    newJObject.insert("Gain",QJsonValue(ui->cbGainConfig->currentText()));
    newJObject.insert("Operational Mode",QJsonValue(ui->cbOperationMode->currentText()));
    newJObject.insert("Data rate",QJsonValue(ui->cbDataRate->currentText()));
    newJObject.insert("Comparator mode",QJsonValue(ui->cbCompMode->currentText()));
    newJObject.insert("comparator polarity",QJsonValue(ui->cbCompPolarity->currentText()));
    newJObject.insert("Comparator latch",QJsonValue(ui->cbCompLatching->currentText()));
    newJObject.insert("Comparator Que",QJsonValue(ui->cbCompQue->currentText()));
    newJObject.insert("XD scale 0%",QJsonValue(ui->dsXDscale0->value()));
    newJObject.insert("XD scale 100%",QJsonValue(ui->dsXDscale100->value()));
    newJObject.insert("PV scale 0%",QJsonValue(ui->dsPVscale0->value()));
    newJObject.insert("PV scale 100%",QJsonValue(ui->dsPVscale100->value()));
    newJObject.insert("PV unit",QJsonValue(ui->lePV_Unit->text()));
    newJObject.insert("Linearization",QJsonValue(ui->cbDataProcessing->currentText()));
    QJsonArray jArray0;
    for(int i=0;i<5;i++)
    {
        jObjectTmp.insert("raw data",QJsonValue(vItemArray[i][0].text()));
        jObjectTmp.insert("Actual value",QJsonValue(vItemArray[i][1].text()));
        jArray0.append(QJsonValue(jObjectTmp));
    }
    newJObject.insert("Calibration data",QJsonValue(jArray0));
    jObject->insert(ui->leTag->text(),QJsonValue(newJObject));
}

ErrorCode ADS111X::json2Config(QJsonObject *jObject)
{
    QJsonObject *jObjectTmp = new QJsonObject(jObject->value(ui->leTag->text()).toObject());
    if(jObjectTmp->value("device").toString()!="ADS111X")
        return Error_DeviceMismatch;
    ui->leIOfile->setText(jObjectTmp->value("I/O file").toString());
    ui->sbAddress->setValue(jObjectTmp->value("address").toInt());
    ui->cbMuxConfig->setCurrentText(jObjectTmp->value("Multiplexer").toString());
    ui->cbGainConfig->setCurrentText(jObjectTmp->value("Gain").toString());
    ui->cbOperationMode->setCurrentText(jObjectTmp->value("Operational Mode").toString());
    ui->cbDataRate->setCurrentText(jObjectTmp->value("Data rate").toString());
    ui->cbCompMode->setCurrentText(jObjectTmp->value("Comparator mode").toString());
    ui->cbCompPolarity->setCurrentText(jObjectTmp->value("comparator polarity").toString());
    ui->cbCompLatching->setCurrentText(jObjectTmp->value("Comparator latch").toString());
    ui->cbCompQue->setCurrentText(jObjectTmp->value("Comparator Que").toString());
    ui->dsXDscale0->setValue(jObjectTmp->value("XD scale 0%").toDouble());
    ui->dsXDscale100->setValue(jObjectTmp->value("XD scale 100%").toDouble());
    ui->dsPVscale0->setValue(jObjectTmp->value("PV scale 0%").toDouble());
    ui->dsPVscale100->setValue(jObjectTmp->value("PV scale 100%").toDouble());
    ui->lePV_Unit->setText(jObjectTmp->value("PV unit").toString());
    QJsonArray jArray0 = jObjectTmp->value("Calibration data").toArray();
    if(jArray0.count()==5)
    {
        for(int i=0;i<5;i++)
        {
            vItemArray[i][0].setText(jArray0[i].toObject().value("raw data").toString());
            vItemArray[i][1].setText(jArray0[i].toObject().value("Actual value").toString());
        }
    }
    FTCoeffCalc();
    ui->cbDataProcessing->setCurrentText(jObjectTmp->value("Linearization").toString());
    return No_Error;
}

uint16_t ADS111X::ConfigAssembly()
{
    return     static_cast<uint16_t>(ui->cbMuxConfig->currentIndex() <<12 |
                                     (ui->cbGainConfig->currentIndex()) << 9 |
                                     (ui->cbOperationMode->currentIndex()) << 8 |
                                     (ui->cbDataRate->currentIndex()) << 5 |
                                     (ui->cbCompMode->currentIndex()) << 4 |
                                     (ui->cbCompPolarity->currentIndex()) << 3 |
                                     (ui->cbCompLatching->currentIndex()) << 2 |
                                     (ui->cbCompQue->currentIndex()));
}

//double ADS111X::Linearize(double pv)
//{
//    if(ui->cbLinearization->currentText()=="Linear")
//        return pv;
//    else
//        return 0;
//}

double ADS111X::DataProcessing(int16_t *data)
{
    switch (ui->cbDataProcessing->currentIndex())
    {
    case DataProcessing_Linear:
        double deltaXD, deltaOUT, k;
        deltaXD = ui->dsXDscale100->value()-ui->dsXDscale0->value();
        deltaOUT = ui->dsPVscale100->value() - ui->dsPVscale0->value();
        if(deltaXD==0)
            return 0;
        k = deltaOUT / deltaXD;
        return (*data - ui->dsXDscale0->value())*k + ui->dsPVscale0->value();
    case DataProcessing_SquerRoot:
        return  0;
    case DataProcessing_CalibrationChart:
        return Convertion_CalibrationChart(data);
    default:
        return 0;
    }

}

double ADS111X::Convertion_CalibrationChart(int16_t *rawData)
{
    double tmpVal = static_cast<double>(*rawData)-vConvertionData.shiftX;
    double Output = tmpVal * vConvertionData.K;
//    double constArg = M_PI/vConvertionData.maxX;
    for(int i=1;i<3;i++)
        Output += vConvertionData.Polinomial_Coefficients[i-1]*sin(vConvertionData.constArg*double(i*(tmpVal)));
    return Output + vConvertionData.shiftY;
}

void ADS111X::FTCoeffCalc()
{
    QList<QPointF>    calibrationChart;
//    double k;

    //clean convertion data
    vConvertionData.K = 0;
    vConvertionData.maxX = 0;
    vConvertionData.maxY = 0;
    vConvertionData.shiftX = 0;
    vConvertionData.K = 0;

    //Translation from UI to Calibration data;
    for(int i=0;i<5;i++)
    {
        QPointF tmpPoint;
        tmpPoint.setX(vItemArray[i][0].text().toDouble());
        tmpPoint.setY(vItemArray[i][1].text().toDouble());
        calibrationChart.append(tmpPoint);
    }

    //shift relative to 0 calculation
    vConvertionData.shiftX = calibrationChart[0].x();
    vConvertionData.shiftY = calibrationChart[0].y();

    //shifting whole chart so the begining is at 0,0 point
    for(int i=0;i<5;i++)
    {
        calibrationChart[i].setX(calibrationChart[i].rx() - vConvertionData.shiftX);
        calibrationChart[i].setY(calibrationChart[i].ry() - vConvertionData.shiftY);
    }

    vConvertionData.maxX = calibrationChart[4].x();
    vConvertionData.maxY = calibrationChart[4].y();
    vConvertionData.K = vConvertionData.maxY/vConvertionData.maxX;

    //substruct the linear part from chart
    for(int i=0;i<5;i++)
    {
        calibrationChart[i].setY( calibrationChart[i].ry() - calibrationChart[i].rx()*vConvertionData.K);
    }

    //calculate polinominal coefficients
    vConvertionData.constArg = M_PI / vConvertionData.maxX;
    for(int i=0;i<2;i++)
    {
        double Bi = 0;
        for(int j=1;j<4;j++)
        {
            Bi += calibrationChart[j].ry()*std::sin((i+1)*calibrationChart[j].rx()*vConvertionData.constArg);
        }
        vConvertionData.Polinomial_Coefficients[i] = Bi * 0.5;
    }
}

void ADS111X::on_sbAddress_valueChanged(int arg1)
{
    vAddress = static_cast<uint8_t>(arg1);
}

void ADS111X::on_chbEditCalibrationData_toggled(bool checked)
{
    if(checked)
        ui->twCalibrationChart->setEnabled(true);
    else
        ui->twCalibrationChart->setEnabled(false);
}

void ADS111X::on_cbDataProcessing_currentIndexChanged(const QString &arg1)
{
    if(arg1=="Calibration Chart")
    {
        ui->dsPVscale0->setEnabled(false);
        ui->dsXDscale0->setEnabled(false);
        ui->dsPVscale100->setEnabled(false);
        ui->dsXDscale100->setEnabled(false);
    }
    else
    {
        ui->dsPVscale0->setEnabled(true);
        ui->dsXDscale0->setEnabled(true);
        ui->dsPVscale100->setEnabled(true);
        ui->dsXDscale100->setEnabled(true);
    }
}

void ADS111X::on_cbDataProcessing_currentIndexChanged(int index)
{
    switch(index)
    {
    case DataProcessing_CalibrationChart:
        ui->dsPVscale0->setEnabled(false);
        ui->dsXDscale0->setEnabled(false);
        ui->dsPVscale100->setEnabled(false);
        ui->dsXDscale100->setEnabled(false);
        ui->dsPVscale0->setValue(vConvertionData.shiftY);
        ui->dsXDscale0->setValue(vConvertionData.shiftX);
        ui->dsPVscale100->setValue(vConvertionData.shiftY + vConvertionData.maxY);
        ui->dsXDscale100->setValue(vConvertionData.shiftX + vConvertionData.maxX);
        break;
    default:
        ui->dsPVscale0->setEnabled(true);
        ui->dsXDscale0->setEnabled(true);
        ui->dsPVscale100->setEnabled(true);
        ui->dsXDscale100->setEnabled(true);
    }
}
